var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var acbSchema = new Schema({
    codigoCepario: { type: String, required: [true, 'El código es necesario'] },
    nombreCientifico: { type: String, required: [true, 'El nombre es necesario'] },
    sinonimo1: { type: String, required: [true, 'El sinonimo1 es necesario'] },
    sinonimo2: { type: String, required: [true, 'El sinonimo2 es necesario'] },
    sinonimo3: { type: String, required: [true, 'El sinonimo3 es necesario'] },
    sector: { type: String, required: [true, 'El sector es necesario'] },
    zona: { type: String, required: [true, 'La zona es necesario'] },
    temperatura: { type: String, required: [true, 'La temperatura es necesario'] },
    altitud: { type: String, required: [true, 'La altitud es necesario'] },
    latitud: { type: String, required: [true, 'La latitud es necesario'] },
    longitud: { type: String, required: [true, 'La longitud es necesario'] },
    colector: { type: String, required: [true, 'La colector es necesario'] },
    fechaColecta: { type: String, required: [true, 'La fecha de colecta es necesario'] },
    ubigeo: { type: String, required: [true, 'El ubigeo es necesario'] },
    agente: { type: Schema.Types.ObjectId, ref: 'Agente' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'acbs' });

module.exports = mongoose.model('Acb', acbSchema);