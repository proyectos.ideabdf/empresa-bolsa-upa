var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var participanteSchema = new Schema({
    nombres: { type: String, required: [true, 'Los nombres son necesarios'] },
    apellidoPaterno: { type: String, required: [true, 'El apellido paterno es necesario'] },
    apellidoMaterno: { type: String, required: [true, 'El apellido materno es necesario'] },
    dni: { type: String, required: false },
    sexo: { type: String, required: false },
    movil: { type: String, required: false },
    fijo: { type: String, required: false },
    direccion: { type: String, required: false },
    distrito: { type: String, required: false },
    provincia: { type: String, required: false },
    region: { type: String, required: false },
    fechaNacimiento: { type: String, required: false },
    distritoNacimiento: { type: String, required: false },
    carreraProfesional: { type: String, required: false },
    anioTerminoPreGrado: { type: String, required: false },
    situacionAcademicaActual: { type: String, required: false },
    estudiosPostGrado: { type: String, required: false },
    situacionLaboral: { type: String, required: false },
    empleado: { type: String, required: false },
    cargo: { type: String, required: false },
    nombreEmpresa: { type: String, required: false },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario', required: true }
}, { collection: 'participantes' });


module.exports = mongoose.model('Participante', participanteSchema);