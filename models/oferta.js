var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var ofertaSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    descripcion: { type: String, required: [true, 'la descripción es necesario'] },
    fechaInicio: { type: String, required: false },
    fechaFin: { type: String, required: false },
    horaInicio: { type: String, required: false },
    horaFin: { type: String, required: false },
    horas: { type: Number, required: false },
    precio: { type: Number, required: false },
    vacantes: { type: Number, required: false },
    inscritos: { type: Number, required: false },
    docente: { type: String, required: false },
    temas: { type: String, required: [true, 'Los temas son necesarios'] },
    fechaCreacion: { type: String, required: false },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: [true, 'El id usuario es un campo obligatorio ']
    },
    curso: {
        type: Schema.Types.ObjectId,
        ref: 'Curso',
        required: [true, 'El id curso es un campo obligatorio ']
    }
}, { collection: 'ofertas' });


module.exports = mongoose.model('Oferta', ofertaSchema);