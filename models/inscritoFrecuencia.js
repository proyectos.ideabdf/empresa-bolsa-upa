var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var inscritoFrecuenciaSchema = new Schema({
    asistencia: { type: Boolean, required: true, default: false },
    participante: { type: Schema.Types.ObjectId, ref: 'Usuario' },
    frecuencia: { type: Schema.Types.ObjectId, ref: 'Frecuencia' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'inscritoFrecuencias' });



module.exports = mongoose.model('InscritoFrecuencia', inscritoFrecuenciaSchema);