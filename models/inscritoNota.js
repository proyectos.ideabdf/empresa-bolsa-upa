var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var inscritoNotaSchema = new Schema({
    notaTeorica: { type: Number, required: false, default: 0 },
    notaInteres: { type: Number, required: false, default: 0 },
    notaAsistencia: { type: Number, required: false, default: 0 },
    notaPractica: { type: Number, required: false, default: 0 },
    notaExamen: { type: Number, required: false, default: 0 },
    notaPromedio: { type: Number, required: false, default: 0 },
    ingreso: { type: Boolean, required: false, default: true },
    inscrito: { type: Schema.Types.ObjectId, ref: 'Inscrito' },
    oferta: { type: Schema.Types.ObjectId, ref: 'Oferta' },
    participante: { type: Schema.Types.ObjectId, ref: 'Participante' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'inscritoNotas' });



module.exports = mongoose.model('InscritoNota', inscritoNotaSchema);