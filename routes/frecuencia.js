var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Frecuencia = require('../models/frecuencia');

// ==========================================
// Obtener todos las frecuencias
// ==========================================
app.get('/:idOferta', (req, res, next) => {
    var idOferta = req.params.idOferta;
    Frecuencia.find({ oferta: idOferta })
        .exec(
            (err, frecuencias) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando frecuencias',
                        errors: err
                    });
                }

                Frecuencia.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        frecuencias: frecuencias,
                        total: conteo
                    });


                });

            });
});

// ===============================================================
// Obtener todos las frecuencias de una determinada fecha y oferta
// ===============================================================

app.get('/:idOferta/:fecha', (req, res, next) => {
    var idOferta = req.params.idOferta;
    var fecha = req.params.fecha;
    Frecuencia.find({ oferta: idOferta, fecha: fecha })
        .exec(
            (err, frecuencias) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando frecuencias',
                        errors: err
                    });
                }

                res.status(200).json({
                    ok: true,
                    frecuencias: frecuencias
                });

            });
});

// ======================================================================
// Obtener todos las frecuencias de una determinada oferta y participante
// ======================================================================

// app.get('/participante/:idOferta/:idParticipante', (req, res, next) => {
//     var idOferta = req.params.idOferta;
//     var idParticipante = req.params.idParticipante;

//     Frecuencia.find({ oferta: idOferta, participante: idParticipante })
//         .exec(
//             (err, frecuencias) => {

//                 if (err) {
//                     return res.status(500).json({
//                         ok: false,
//                         mensaje: 'Error cargando frecuencias',
//                         errors: err
//                     });
//                 }

//                 res.status(200).json({
//                     ok: true,
//                     frecuencias: frecuencias
//                 });

//             });
// });

// ==========================================
// Actualizar frecuencia
// ==========================================

app.put('/:idFrecuencia', mdAutenticacion.verificaToken, (req, res) => {

    var idFrecuencia = req.params.idFrecuencia;
    var body = req.body;

    Frecuencia.findById(idFrecuencia, (err, frecuencia) => {


        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar frecuencia',
                errors: err
            });
        }

        if (!frecuencia) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El usuario con el id ' + idFrecuencia + ' no existe',
                errors: { message: 'No existe un usuario con ese ID' }
            });
        }

        frecuencia.fecha = body.fecha;
        frecuencia.asistencia = body.asistencia;

        frecuencia.save((err, frecuenciaGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar usuario',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                frecuencia: frecuenciaGuardado
            });

        });

    });

});



// ==========================================
// Crear un nueva frecuencia
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var frecuencia = new Frecuencia({
        fecha: body.fecha,
        asistencia: body.asistencia,
        oferta: body.oferta,
        usuario: req.usuario._id
    });

    frecuencia.save((err, frecuenciaGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear frecuencia',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            frecuencia: frecuenciaGuardado
        });


    });

});


// ============================================
//   Borrar un usuario por el id
// ============================================
app.delete('/:idFrecuencia', mdAutenticacion.verificaToken, (req, res) => {

    var idFrecuencia = req.params.idFrecuencia;

    Frecuencia.findByIdAndRemove(idFrecuencia, (err, frecuenciaBorrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar frecuencia',
                errors: err
            });
        }

        if (!frecuenciaBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un frecuencia con ese id',
                errors: { message: 'No existe un frecuencia con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            frecuencia: frecuenciaBorrado
        });

    });

});


module.exports = app;