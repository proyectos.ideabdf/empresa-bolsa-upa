var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Nivel4 = require('../models/nivel4');

// ==========================================
// Obtener todos las niveles4
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Nivel4.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .exec(
            (err, niveles4) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando nivel4',
                        errors: err
                    });
                }

                Nivel4.count({}, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        niveles4: niveles4,
                        total: conteo
                    });
                })

            });
});

// ==========================================
//  Obtener Nivel4 por ID
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Nivel4.findById(id)
        .populate('usuario', 'nombre img email')
        .exec((err, nivel4) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar nivel4',
                    errors: err
                });
            }

            if (!nivel4) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El nivel4 con el id ' + id + 'no existe',
                    errors: { message: 'No existe un nivel4 con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                nivel4: nivel4
            });
        })
})





// ==========================================
// Actualizar Nivel4
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Nivel4.findById(id, (err, nivel4) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar nivel4',
                errors: err
            });
        }

        if (!nivel4) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El nivel4 con el id ' + id + ' no existe',
                errors: { message: 'No existe un nivel4 con ese ID' }
            });
        }


        nivel4.nombre = body.nombre;
        nivel4.usuario = req.usuario._id;

        nivel4.save((err, nivel4Guardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar nivel4',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                nivel4: nivel4Guardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo nivel4
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var nivel4 = new Nivel4({
        nombre: body.nombre,
        usuario: req.usuario._id
    });

    nivel4.save((err, nivel4Guardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear nivel4',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            nivel4: nivel4Guardado
        });


    });

});


// ============================================
//   Borrar un nivel4 por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Nivel4.findByIdAndRemove(id, (err, nivel4Borrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar nivel4',
                errors: err
            });
        }

        if (!nivel4Borrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nivel4 con ese id',
                errors: { message: 'No existe un nivel4 con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            nivel4: nivel4Borrado
        });

    });

});


module.exports = app;