var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Usuario = require('../models/usuario');
var Participante = require('../models/usuario');
var Inscrito = require('../models/inscrito');
var Oferta = require('../models/oferta');
var Curso = require('../models/curso');

// var participante = new Participante();

// ==========================================
// Obtener todos los inscritos de oferta
// ==========================================
app.get('/inscritos/:idOferta', (req, res, next) => {
    var idOferta = req.params.idOferta;
    var desde = req.query.desde || 0;
    desde = Number(desde);

    Inscrito.find({ oferta: idOferta })
        .skip(desde)
        .limit(5)
        .populate('participante', 'nombre email img estadoParticipante')
        .populate('oferta')
        .populate('usuario', 'nombre email img estadoParticipante')
        .exec(
            (err, inscritos) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando inscritos',
                        errors: err
                    });
                }

                Inscrito.count({ oferta: idOferta }, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        inscritos: inscritos,
                        total: conteo
                    });

                });
            });
});


// ==========================================
// Obtener todos los inscritos de oferta
// ==========================================
app.get('/inscritosNota/:idOferta', (req, res, next) => {
    var idOferta = req.params.idOferta;
    var desde = req.query.desde || 0;
    desde = Number(desde);

    InscritoNota.find({ oferta: idOferta })
        .skip(desde)
        .limit(5)
        .populate('participante', 'nombre email img estadoParticipante')
        .populate('oferta')
        .populate('inscrito')
        .populate('usuario', 'nombre email img estadoParticipante')
        .exec(
            (err, inscritosNota) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando inscritos',
                        errors: err
                    });
                }

                Inscrito.count({ oferta: idOferta }, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        inscritosNota: inscritosNota,
                        total: conteo
                    });

                });
            });
});


// ==========================================
//  Obtener Inscripciones por ID
// ==========================================
app.get('/:idParticipante', (req, res) => {

    var idParticipante = req.params.idParticipante;
    var desde = req.query.desde || 0;
    desde = Number(desde);

    Inscrito.find({ participante: idParticipante })
        .skip(desde)
        .limit(5)
        .populate('participante', 'nombre email img estadoParticipante')
        .populate('oferta')
        .populate('usuario', 'nombre email img estadoParticipante')
        .exec(
            (err, inscritos) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando inscritos',
                        errors: err
                    });
                }

                Inscrito.count({ participante: idParticipante }, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        inscritos: inscritos,
                        total: conteo
                    });

                });
            });
});

// ==========================================
//  Obtener Inscripcion por ID + ID Oferta
// ==========================================
app.get('/:idParticipante/:idOferta', (req, res) => {

    var idParticipante = req.params.idParticipante;
    var idOferta = req.params.idOferta;

    Inscrito.find({ participante: idParticipante, oferta: idOferta })
        .populate('participante', 'nombre email img estadoParticipante')
        .populate('oferta')
        .populate('usuario', 'nombre email img estadoParticipante')
        .exec((err, inscrito) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar inscripcion',
                    errors: err
                });
            }

            if (!inscrito) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'La inscripcion con el id ' + id + 'no existe',
                    errors: { message: 'No existe un inscripcion con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                inscrito: inscrito
            });
        })
})


// ==========================================
// Actualizar inscrito
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Inscrito.findById(id, (err, inscrito) => {


        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar inscrito',
                errors: err
            });
        }

        if (!inscrito) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El inscrito con el id ' + id + ' no existe',
                errors: { message: 'No existe un inscrito con ese ID' }
            });
        }


        inscrito.estadoInscrito = body.estadoInscrito;

        inscrito.save((err, inscritoGuardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar inscrito',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                inscrito: inscritoGuardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo inscrito
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var inscrito = new Inscrito({
        estadoInscrito: body.estadoInscrito,
        participante: body.participante,
        oferta: body.oferta,
        usuario: req.usuario._id
    });

    inscrito.save((err, inscritoGuardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al inscribir',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            inscrito: inscritoGuardado
        });


    });

});


module.exports = app;